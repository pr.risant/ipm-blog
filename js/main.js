
$( document ).ready(function() {
    
    $(".overlay").hide();

    $("#nav-open").click(function() {
        $(".overlay" ).fadeIn(600);
        $("body").addClass("noScroll");
    });

    $("#nav-close").click(function() {
        $( ".overlay" ).fadeOut(600);
        $("body").removeClass("noScroll");
    });

    $("body").css("display", "none");
 
    $("body").delay(200).fadeIn(600);

    $(function() {
        $("img.lazy").lazyload({
            effect : "fadeIn",
            threshold : 600
        });
    });
 
    $(".link").click(function(event){
        event.preventDefault();
        linkLocation = this.href;
        $("body").fadeOut(300, redirectPage);      
    });
         
    function redirectPage() {
        window.location = linkLocation;
    }

    //$("body").css("display", "none");
 
    //$("body").fadeIn(600);
    
    $("#top").click(function () {
        $("html, body").animate({scrollTop: 0}, 1000, 'easeInOutQuart');
     });

$('#main-image').height($(window).height())

    function slideContent() {
    var target = $('#content');
    if (target.length)
    {
        var top = target.offset().top;
        $('html,body').animate({scrollTop: top}, 1000, 'easeInOutQuart');
        return false;
    }
    }

$('a[href=#content]').click(function() {slideContent(); return false});

});



// for the window resize
$(window).resize(function() {
    $('#main-image').height($(window).height())
});
